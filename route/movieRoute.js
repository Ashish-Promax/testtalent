const express = require('express');
const router = express.Router();
const expressJoi = require('express-joi-validator');
const movieJoiModel=require('../joi-models/joi-movie')
const movieController = require('../controller/movieController');

router.post('/movie',expressJoi(movieJoiModel.movie),movieController.addMovie);
router.post('/movies',expressJoi(movieJoiModel.movies),movieController.addMultipleMovie)
router.get('/movies',expressJoi(movieJoiModel.movieSearch),movieController.movieSearch)


/* This route is used to handle joi error */
router.use(function (err, req, res, next) {
  if (err.isBoom) {
    res.status(400).json({ message: err.data[0].message.replace(/['"]+/g, '') });
  }
});

module.exports = router;