/**
 * @author Ashish Shende
 *
 * This is the basic configuration of the Node.js express server
 */

const express = require('express');
const bodyParser = require('body-parser');

const movieRoute = require('./route/movieRoute');


// Mongoose
require('./db/mongoose');
require('./index');

const app = express();

// CORS headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token');
    // Handle preflight OPTIONS request
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// Parse application/json
app.use(bodyParser.json());

// Routes



/*version 2 Routes*/
app.use('/api/v1',movieRoute);


module.exports = app