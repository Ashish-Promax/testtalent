var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var movieSchema = new Schema({
    name: { 
        type: String, 
        trim: true, 
        unique:true,
        required: [true, 'Movies Name cannot be blank'] 
    },
    year: { 
        type: Date, 
        required: [true, 'Year cannot be blank'] 
    },
    director: { 
        type: String, 
        required: [true, 'Director cannot be blank'] 
    },
    headers: {type:Object}
}, { timestamps: true });

module.exports = mongoose.model('movie', movieSchema);