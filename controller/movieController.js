const movieModel = require('../model/movie')

exports.addMovie = (req, res, next) => {
  movieModel.create(req.body).then(result => {
    res.status(200).json({
      message: "successfully save the movies",
      data: result
    })
  }).catch(err => {

    if (err.keyPattern.name == 1) {
      res.status(409).send({
        message: "Duplicate name"
      });
    }

    res.status(500).json({
      message: "something went wrong"
    })
  })
}

exports.addMultipleMovie = (req, res, next) => {
  if (req.body.length != 0) {
    movieModel.insertMany(req.body,{ordered : false }).then(result => {
      res.status(200).json({
        message: "successfully save the movies",
        data: result
      })
    }).catch(err => {
      if (err.name == "BulkWriteError"){
        let insertedDocs = err.insertedDocs;
        if (insertedDocs.length == 0){
          res.status(200).json({
            message: "Duplicate entries were not inserted",
            data: {
              insertedEntries: insertedDocs,
              duplicateEntries: req.body
            }
          })  
        } else {
          let tempArray =objectsAreSame(insertedDocs,req.body)
          res.status(200).json({
            message: "Duplicate entries were not inserted",
            data: {
              insertedEntries: insertedDocs,
              duplicateEntries: tempArray
            }
          })
        }
      } else {
        res.status(500).json({
          message: "something went wrong",
          error: err.message
        })
      }
    })
  } else {
    res.status(202).json({
      message: "Please the required infomation"
    })
  }
}

function objectsAreSame(x, y) {
  return  y.filter(({ name: id1 }) => !x.some(({ name: id2 }) => id2 === id1));
}

exports.movieSearch = (req, res, next) => {
  movieModel.find({ 'name': req.query.query }).then(result => {
    if (result.length != 0) {
      res.status(200).json({
        'data': result
      })
    } else {
      res.status(202).json({
        message: "No record found"
      })
    }
  }).catch(err => {
    res.status(404).json({
      message: "something went wrong"
    })
  })

}