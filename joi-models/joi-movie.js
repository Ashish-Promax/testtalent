/**
* @author Ashish Shende
*
* This file is used to validate user Factories APIs.
*/

const Joi = require('joi');

exports.movie = {
    body: {
        'name':Joi.string().trim().required().label("Name"),
        "year":Joi.string().required().label("Year"),
        "director": Joi.string().trim().required().label(" director"),
    },
    params: {},
    query: {}
}

exports.movies = {
    body:Joi.array().required()
}

exports.movieSearch = {
    body: {
    },
    params: {},
    query: {
        query:Joi.string().trim().required().label('Query')
    }
}

