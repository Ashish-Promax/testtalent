/**
 * @author Ashish Shende
 * 
 * MongoDB config file
 */

const mongoose = require('mongoose');
const config = require('config');
const mongoDbConfig = config.get('MongoDB');

mongoConnUrl = "";

if (process.env.NODE_ENV == undefined || process.env.NODE_ENV == "develop") {
   // mongoConnUrl = "mongodb://" + mongoDbConfig.host + ":" + mongoDbConfig.port + "/" + mongoDbConfig.dbName
   mongoConnUrl = "mongodb+srv://TalentCornorTest-12:"+mongoDbConfig.password+"@cluster0.4ss1z.mongodb.net/"+mongoDbConfig.dbName+"?retryWrites=true&w=majority";
} else {
   // mongoConnUrl = "mongodb://" + mongoDbConfig.user + ":" + mongoDbConfig.password + 
   // "@" + mongoDbConfig.host + ":" + mongoDbConfig.port +  "/" + mongoDbConfig.dbName;
   mongoConnUrl = "mongodb+srv://TalentCornorTest-12:"+mongoDbConfig.password+"@cluster0.4ss1z.mongodb.net/"+mongoDbConfig.dbName+"?retryWrites=true&w=majority";
}

// Connect to MongoDB
mongoose.connect(mongoConnUrl, {
   useCreateIndex: true,
   useNewUrlParser: true,
   useUnifiedTopology: true
});

// When connected
mongoose.connection.on('connected', function () {
   console.log('Mongoose default connection open on ' + mongoConnUrl);
  
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
   console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
   console.log('Mongoose default connection disconnected');
});


module.exports = { mongoose };